# Flectra Community / dms

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[dms](dms/) | 2.0.4.2.3| Document Management System for Odoo
[dms_field](dms_field/) | 2.0.1.0.0|         Create DMS View and allow to use them inside a record


